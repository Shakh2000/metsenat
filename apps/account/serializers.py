from rest_framework import serializers
from apps.account.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ("id", "username", "image", "first_name", "last_name", "phone_number")

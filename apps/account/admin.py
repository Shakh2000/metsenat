from django.contrib import admin
from django.utils.text import gettext_lazy as _
from apps.account.models import CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    fieldsets = (
        (_('Main'), {'fields': ('username', 'password')}),
        (_('Personal info'),
         {'fields': ('first_name', 'last_name', 'phone_number', 'image', 'is_active')}),
    )

    list_display = ['id', 'phone_number', 'first_name', 'username']
    list_display_links = ['id', 'phone_number', 'first_name', 'username']

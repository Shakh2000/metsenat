from drf_yasg import openapi


def get_username_password():
    username = openapi.Parameter('username', openapi.IN_QUERY, description="Enter name",
                                 type=openapi.TYPE_STRING)
    password = openapi.Parameter("password", openapi.IN_QUERY, description="Enter password",
                                 type=openapi.TYPE_STRING)
    return [username, password]

from rest_framework.views import APIView, status, Response
from apps.account.models import CustomUser
from drf_yasg.utils import swagger_auto_schema
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from apps.account import filter_params
from apps.account.serializers import UserSerializer
from hashlib import sha256

class RegisterView(APIView):
    """Registration with email"""
    @swagger_auto_schema(manual_parameters=filter_params.get_username_password())
    def post(self, request, *args, **kwargs):
        username = request.data.get("username", None) or request.query_params.get("username", None)
        password = request.data.get("password", None) or request.query_params.get("password", None)
        if username:
            try:
                CustomUser.objects.get(username=username)
                return Response({"error": "This username is already linked to an existing account"},
                                status=status.HTTP_400_BAD_REQUEST)
            except:
                user = CustomUser.objects.create_user(username=username, password=password)
                user.is_superuser = True
                user.save()
            return Response({"success": "User found"}, status=status.HTTP_200_OK)
        return Response({"error": "Login or password was not provided"}, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
    """Verification user account"""
    @swagger_auto_schema(manual_parameters=filter_params.get_username_password())
    def post(self, request, *args, **kwargs):
        login = request.data.get("username", None) or request.query_params.get("username", None)
        password = request.data.get("password", None) or request.query_params.get("password", None)
        if login and password:
            print(sha256(password.encode()).hexdigest())
            try:
                user = CustomUser.objects.get(username=login)
                if user:
                    if user.password is not None:
                        if user.check_password(password):
                            user.is_active = True
                            user.save()
                            refresh = TokenObtainPairSerializer().get_token(user)
                            user_data = UserSerializer(instance=user).data
                            data = {"refresh": str(refresh), "access": str(refresh.access_token), "user": user_data}
                            return Response(data=data, status=status.HTTP_201_CREATED)
                        return Response(
                            {"error": "wrong password"}, status=status.HTTP_403_FORBIDDEN
                        )
                    return Response(
                        {"error": "Please enter password."},
                        status=status.HTTP_403_FORBIDDEN
                    )
            except:
                return Response(
                    {"error": f"User with {login} is not found."},
                    status=status.HTTP_404_NOT_FOUND
                )
        return Response(
            {"error": "Login or password not provided."},
            status=status.HTTP_400_BAD_REQUEST
        )

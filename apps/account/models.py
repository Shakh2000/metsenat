import os
from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    username = models.CharField(max_length=50, blank=True, null=True, unique=True)
    image = models.ImageField(upload_to='users')
    phone_number = models.CharField(max_length=13, blank=True, null=True)
    password = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.first_name if self.first_name else self.phone_number}"

    def delete(self, using=None, keep_parents=False):
        absolute_path_of_image = os.path.abspath(f'media/{self.image}')
        try:
            os.remove(absolute_path_of_image)
        except os.error:
            pass
        super(CustomUser, self).delete()

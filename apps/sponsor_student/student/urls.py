from django.urls import path
from apps.sponsor_student.student import views
from rest_framework.routers import SimpleRouter
router = SimpleRouter()
router.register("", views.StudentView)

urlpatterns = [
    path("sponsor/", views.StudentSponsorsView.as_view({"get": "list"})),
    path("sponsor/create", views.StudentSponsorsView.as_view({"post": "create"})),
    path("sponsor/patch/", views.StudentSponsorsView.as_view({"patch": "update"})),
    path("sponsor/delete/<int:pk>", views.StudentSponsorsView.as_view({"delete": "destroy"}))
] + router.urls

from rest_framework.viewsets import ModelViewSet
from rest_framework.views import Response, status
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from django.shortcuts import get_object_or_404
from django.db.models import Sum, F
from apps.sponsor_student.student import filter_params
from apps.sponsor_student import models
from apps.sponsor_student.student import serializers


class StudentView(ModelViewSet):
    queryset = models.Student.objects.annotate(all_amount=Sum(F('student__allocated_amount')))
    serializer_class = serializers.StudentSerializer
    # permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        univ_degree = self.request.query_params.get("degree")
        university = self.request.query_params.get("university")
        filter_dict = {}
        if univ_degree:
            filter_dict["univ_degree"] = univ_degree
        if university:
            filter_dict["university"] = university
        queryset = queryset.filter(**filter_dict).order_by("id")
        return queryset
    
    @swagger_auto_schema(manual_parameters=filter_params.get_filter_student())
    def list(self, request, *args, **kwargs):
        return super(StudentView, self).list(kwargs)


class StudentSponsorsView(ModelViewSet):
    queryset = models.StudentSponsor.objects.all()
    serializer_class = serializers.StudentSponsorSerializer
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        if self.action == 'list':
            return serializers.StudentSponsorListSerializer
        return self.serializer_class

    def update(self, request, *args, **kwargs):
        amount = self.request.data.get("allocated_amount")
        student = self.request.data.get("student")
        sponsor = self.request.data.get("sponsor")
        sponsor_object = get_object_or_404(models.StudentSponsor, student_id=sponsor, sponsor_id=sponsor)
        if amount:
            sponsor_object.allocated_amount = float(amount)
        sponsor_object.save()
        data = serializers.StudentSponsorSerializer(instance=sponsor_object).data
        return Response(data=data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        sponsors = models.StudentSponsor.objects.filter(sponsor_id=kwargs["pk"])
        sponsors.delete()
        return Response({"success": "Sponsor deleted."}, status=status.HTTP_200_OK)

    def get_queryset(self):
        queryset = super().get_queryset()
        student = self.request.query_params.get("id")
        if student:
            sponsors = queryset.filter(student_id=student).values_list("sponsor", flat=True)
            return models.Sponsor.objects.filter(id__in=sponsors).annotate(allocated_amounts=
                                                                           Sum(F('sponsor__allocated_amount')))
        return []

    @swagger_auto_schema(manual_parameters=filter_params.get_student_id())
    def list(self, request, *args, **kwargs):
        return super(StudentSponsorsView, self).list(kwargs)
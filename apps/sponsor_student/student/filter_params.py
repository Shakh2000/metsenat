from drf_yasg import openapi


def get_filter_student():
    degree = openapi.Parameter('degree', openapi.IN_QUERY, description="Enter degree",
                               type=openapi.TYPE_STRING)
    university = openapi.Parameter("university", openapi.IN_QUERY, description="Enter university",
                                   type=openapi.TYPE_STRING)
    return [degree, university]


def get_student_id():
    id = openapi.Parameter('id', openapi.IN_QUERY, description="Enter student id",
                           type=openapi.TYPE_NUMBER)
    return [id]

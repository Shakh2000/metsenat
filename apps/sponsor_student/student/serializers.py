from rest_framework import serializers
from apps.sponsor_student import models


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Student
        fields = "__all__"

    def to_representation(self, instance):
        data = super(StudentSerializer, self).to_representation(instance)
        try:
            data["all_amount"] = instance.all_amount
        except:
            data["all_amount"] = 0.0
        data["university"] = UniversitySerializer(instance=instance.university).data
        data["created_at"] = instance.created_at.strftime("%d.%m.%Y")
        return data


class StudentSponsorListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Sponsor
        fields = ("id", "first_name", "last_name", "middle_name")

    def to_representation(self, instance):
        data = super(StudentSponsorListSerializer, self).to_representation(instance)
        data["allocated_amount"] = instance.allocated_amounts
        return data


class StudentSponsorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StudentSponsor
        fields = "__all__"


class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.University
        fields = "__all__"

from django.apps import AppConfig


class SponsorStudentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.sponsor_student'

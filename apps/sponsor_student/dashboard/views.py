from rest_framework.views import APIView, Response, status
from rest_framework.permissions import IsAuthenticated
from django.db.models import Sum
from apps.sponsor_student import models
from django.db import connection


def my_custom_sql(self):
    with connection.cursor() as cursor:
        cursor.execute('''Select COALESCE(l1.created_at, l2.created_at), COALESCE(l1.STUDENT_NUMBER, 0) 
                          As student_number, COALESCE(l2.SPONSOR_NUMBER, 0) As sponsor_number
                          From (Select to_char(lh.CREATED_AT, 'dd.mm.yyyy') created_at,
                          Count(*) student_number
                          From sponsor_student_student lh
                          Group By to_char(lh.CREATED_AT, 'dd.mm.yyyy')
                          Order By to_char(lh.CREATED_AT, 'dd.mm.yyyy')) l1
                          FULL OUTER JOIN 
                          (Select to_char(lh.CREATED_AT, 'dd.mm.yyyy') created_at,
                          Count(*) sponsor_number
                          From sponsor_student_sponsor lh
                          Group By to_char(lh.CREATED_AT, 'dd.mm.yyyy')
                          Order By to_char(lh.CREATED_AT, 'dd.mm.yyyy')) l2 
                          On l1.CREATED_AT = l2.CREATED_AT;''')
        row = cursor.fetchall()
    return row


class DashboardView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        main = models.StudentSponsor.objects.aggregate(spent_money=Sum("allocated_amount"))
        main.update(models.Student.objects.aggregate(total_contract=Sum("contract_amount")))
        if main["spent_money"] is None:
            main["spent_money"] = 0
        if main["total_contract"] is None:
            main["total_contract"] = 0
        if main["total_contract"] and main["spent_money"]:
            main["money_needed"] = main["total_contract"] - main["spent_money"]
        return Response(data=main, status=status.HTTP_200_OK)


class DashboardLineGraph(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        sql_data = my_custom_sql(self)
        data = []
        line = {}
        for i in range(len(sql_data)):
            line["date"] = sql_data[i][0]
            line["student_number"] = sql_data[i][1]
            line["sponsor_number"] = sql_data[i][2]
            data.append(line)
            line = {}
        return Response(data=data, status=status.HTTP_200_OK)

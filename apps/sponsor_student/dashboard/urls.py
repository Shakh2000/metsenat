from django.urls import path
from apps.sponsor_student.dashboard import views

urlpatterns = [
    path("", views.DashboardView.as_view()),
    path("line/graph", views.DashboardLineGraph.as_view()),
]

university = [
    {
      "OTM": "Andijon davlat universiteti",
    },
    {
      "OTM": "Toshkent davlat o'zbek tili va adabiyoti universiteti",
    },
    {
      "OTM": "Andijon mashinasozlik instituti",
    },
    {
      "OTM": "Buxoro davlat universiteti",
    },
    {
      "OTM": "Buxoro muhandislik-texnologiya instituti",
    },
    {
      "OTM": "O'zbekiston xalqaro islom akademiyasi",
    },
    {
      "OTM": "Guliston davlat universiteti",
    },
    {
      "OTM": "Jizzax politexnika instituti",
    },
    {
      "OTM": "Namangan davlat universiteti",
    },
    {
      "OTM": "Namangan muhandislik-qurilish instituti",
    },
    {
      "OTM": "Namangan muhandislik-texnologiya instituti",
    },
    {
      "OTM": "Samarqand davlat universiteti",
    },
    {
      "OTM": "Samarqand davlat arxitektura-qurilish instituti",
    },
    {
      "OTM": "O'zbekiston milliy universiteti",
    },
    {
      "OTM": "Toshkent davlat texnika universiteti",
    },
    {
      "OTM": "Navoiy davlat konchilik instituti Nukus filiali",
    },
    {
      "OTM": "Geologiya fanlari universiteti",
    },
    {
      "OTM": "Navoiy davlat konchilik instituti,"
    },
    {
      "OTM": "Toshkent davlat iqtisodiyot universiteti",
    },
    {
      "OTM": "O'zbekiston davlat jahon tillari universiteti",
    },
    {
      "OTM": "Toshkent davlat sharqshunoslik universiteti",
    },
    {
      "OTM": "Toshkent arxitektura-qurilish instituti",
    },
    {
      "OTM": "Milliy rassomlik va dizayn instituti",
    },
    {
      "OTM": "Toshkent to'qimachilik va yengil sanoat instituti",
    },
    {
      "OTM": "Toshkent davlat transport universiteti",
    },
    {
      "OTM": "Toshkent kimyo-texnologiya instituti Shahrisabz filiali",
    },
    {
      "OTM": "Toshkent moliya instituti",
    },
    {
      "OTM": "Toshkent kimyo-texnologiya instituti",
    },
    {
      "OTM": "Toshkent tibbiyot akademiyasi Urganch filiali",
    },
    {
      "OTM": "Termiz davlat universiteti",
    },
    {
      "OTM": "Urganch davlat universiteti",
    },
    {
      "OTM": "Farg'ona davlat universiteti",
    },
    {
      "OTM": "Farg'ona politexnika instituti",
    },
    {
      "OTM": "Toshkent tibbiyot akademiyasi Farg'ona filiali",
    },
    {
      "OTM": "Qarshi davlat universiteti",
    },
    {
      "OTM": "Qoraqalpoq davlat universiteti",
    },
    {
      "OTM": "Samarqand davlat chet tillar instituti",
    },
    {
      "OTM": "Denov tadbirkorlik va pedagogika instituti",
    },
    {
      "OTM": "Qoraqalpog‘iston tibbiyot instituti",
    },
    {
      "OTM": "Jahon iqtisodiyoti va diplomatiya universiteti",
    },
    {
      "OTM": "Jizzax davlat pedagogika instituti",
    },
    {
      "OTM": "Nukus davlat pedagogika instituti",
    },
    {
      "OTM": "Navoiy davlat pedagogika instituti",
    },
    {
      "OTM": "Toshkent viloyati Chirchiq davlat pedagogika instituti",
    },
    {
      "OTM": "Toshkent davlat pedagogika universiteti",
    },
    {
      "OTM": "Qo'qon davlat pedagogika instituti",
    },
    {
      "OTM": "Toshkent davlat pedagogika universiteti Termiz filiali",
    },
    {
      "OTM": "Toshkent davlat pedagogika universiteti Shahrisabz filiali",
    },
    {
      "OTM": "Toshkent davlat iqtisodiyot universiteti Samarqand filiali",
    },
    {
      "OTM": "Andijon tibbiyot instituti",
    },
    {
      "OTM": "Buxoro tibbiyot instituti",
    },
    {
      "OTM": "Samarqand tibbiyot instituti",
    },
    {
      "OTM": "Toshkent tibbiyot akademiyasi",
    },
    {
      "OTM": "Tashkent davlat texnika universitetining Olmaliq filiali",
    },
    {
      "OTM": "Toshkent davlat stomatologiya instituti",
    },
    {
      "OTM": "Toshkent tibbiyot pediatriya instituti",
    },
    {
      "OTM": "Toshkent tibbiyot akademiyasi Termiz filiali",
    },
    {
      "OTM": "Toshkent farmatsevtika instituti",
    },
    {
      "OTM": "Toshkent davlat texnika universiteti Qoʻqon filiali",
    },
    {
      "OTM": "Andijon qishloq ho'jaligi va agrotexnologiyalar instituti",
    },
    {
      "OTM": "Samarqand veterinariya meditsinasi instituti",
    },
    {
      "OTM": "Toshkent davlat agrar universiteti",
    },
    {
      "OTM": "Toshkent davlat agrar universiteti Termiz filiali",
    },
    {
      "OTM": "Qarshi muhandislik - iqtisodiyot instituti",
    },
    {
      "OTM": "Toshkent irrigatsiya va qishloq xo'jaligini mexanizatsiyalash muhandislari instituti",
    },
    {
      "OTM": "O`zbekiston davlat san'at va madaniyat instituti",
    },
    {
      "OTM": "Toshkent davlat texnika universiteti Termiz filiali",
    },
    {
      "OTM": "Toshkent axborot texnologiyalari universiteti",
    },
    {
      "OTM": "O‘zbekiston davlat sanʼat va madaniyat instituti (Farg‘ona mintaqaviy filiali.)",
    },
    {
      "OTM": "O'zbekiston  davlat jismoniy tarbiya va sport universiteti",
    },
    {
      "OTM": "Toshkent davlat yuridik universiteti",
    },
    {
      "OTM": "Samarqand iqtisodiyot va servis instituti",
    },
    {
      "OTM": "Toshkent davlat agrar universiteti Nukus filiali",
    },
    {
      "OTM": "Samarqand veterinariya meditsinasi instituti (Toshkent filiali.)",
    },
    {
      "OTM": "Toshkent axborot texnologiyalari universiteti Samarqand filiali",
    },
    {
      "OTM": "Toshkent axborot texnologiyalari universiteti Qarshi filiali",
    },
    {
      "OTM": "Toshkent axborot texnologiyalari universiteti Urganch filiali",
    },
    {
      "OTM": "Toshkent axborot texnologiyalari universiteti Nukus filiali",
    },
    {
      "OTM": "Toshkent axborot texnologiyalari universiteti Farg'ona filiali",
    },
    {
      "OTM": "O`zbekiston davlat san'at va madaniyat instituti Nukus filiali",
    },
    {
      "OTM": "Toshkent irrigatsiya va qishloq xo'jaligini mexanizatsiyalash muhandislari instituti Buxoro filiali",
    },
    {
      "OTM": "O'zbekiston jurnalistika va ommaviy kommunikatsiyalar universiteti",
    },
    {
      "OTM": "\"Ipak yo'li\" turizm xalqaro universiteti",
    },
    {
      "OTM": "Toshkent irrigatsiya va qishloq xoʻjaligini mexanizatsiyalash muhandislari instituti Qarshi filiali",
    },
    {
      "OTM": "Samarqand veterinariya meditsinasi instituti Nukus filiali",
    },
    {
      "OTM": "Oʻzbekiston  davlat jismoniy tarbiya va sport universiteti Nukus filiali",
    },
    {
      "OTM": "Toshkent davlat yuridik universiteti Ixtisoslashtirilgan filiali",
    },
    {
      "OTM": "O'zbekiston milliy universiteti Jizzax filiali",
    },
    {
      "OTM": "Toshkent kimyo-texnologiya instituti Yangiyer filiali",
    }
]

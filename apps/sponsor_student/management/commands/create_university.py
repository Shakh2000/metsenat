from django.core.management.base import BaseCommand
from apps.sponsor_student.management.commands.universities import university
from apps.sponsor_student.models import University


class Command(BaseCommand):
    help = 'Creating all regions and districs'

    def handle(self, *args, **options):
        University.objects.bulk_create([University(university=reg["OTM"]) for reg in university])
        return "Success"

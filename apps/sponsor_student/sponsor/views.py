from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from apps.sponsor_student import models
from apps.sponsor_student.sponsor import serializers
from apps.sponsor_student.sponsor import filter_params
from django.db.models import Sum, F


class SponsorView(ModelViewSet):
    serializer_class = serializers.SponsorSerializer
    queryset = models.Sponsor.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super().get_queryset()
        app_status = self.request.query_params.get("status")
        amount = self.request.query_params.get("amount")
        year = self.request.query_params.get("year")
        month = self.request.query_params.get("month")
        day = self.request.query_params.get("day")
        filter_dict = {}
        if app_status:
            filter_dict["status"] = app_status
        if amount:
            filter_dict["allocated_amount__lt"] = amount
        if year:
            filter_dict["created_at__year"] = year
        if month:
            filter_dict["created_at__month"] = month
        if day:
            filter_dict["created_at__day"] = day
        queryset = queryset.filter(**filter_dict).order_by("-allocated_amount").\
            annotate(spent_amount=Sum(F('sponsor__allocated_amount'))).order_by("id")
        return queryset

    @swagger_auto_schema(manual_parameters=filter_params.get_filter_sponsor())
    def list(self, request, *args, **kwargs):
        return super(SponsorView, self).list(kwargs)

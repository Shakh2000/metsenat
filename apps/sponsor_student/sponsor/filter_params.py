from drf_yasg import openapi


def get_filter_sponsor():
    status = openapi.Parameter('status', openapi.IN_QUERY, description="Enter status",
                               type=openapi.TYPE_STRING)
    amount = openapi.Parameter("amount", openapi.IN_QUERY, description="Enter amount",
                               type=openapi.TYPE_NUMBER)
    year = openapi.Parameter("year", openapi.IN_QUERY, description="Enter year",
                             type=openapi.TYPE_NUMBER)
    month = openapi.Parameter("month", openapi.IN_QUERY, description="Enter month",
                              type=openapi.TYPE_NUMBER)
    day = openapi.Parameter("day", openapi.IN_QUERY, description="Enter day",
                            type=openapi.TYPE_NUMBER)

    return [status, amount, year, month, day]

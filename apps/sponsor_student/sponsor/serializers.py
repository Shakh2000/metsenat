from rest_framework import serializers
from apps.sponsor_student import models


class SponsorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Sponsor
        fields = "__all__"

    def to_representation(self, instance):
        data = super(SponsorSerializer, self).to_representation(instance)
        data["created_at"] = instance.created_at.strftime("%d.%m.%Y")
        data["spent_amount"] = instance.allocated_amount - instance.balance
        return data

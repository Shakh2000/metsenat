from django.urls import path
from apps.sponsor_student.sponsor import views
from rest_framework.routers import SimpleRouter
router = SimpleRouter()
router.register("", views.SponsorView)

urlpatterns = [

] + router.urls

from django.contrib import admin
from apps.sponsor_student import models


@admin.register(models.Sponsor)
class SponsorAdmin(admin.ModelAdmin):
    # form = SponsorForm
    list_display = ["id", "first_name", "last_name", "phone_number", "status"]
    list_display_links = ["id", "first_name", "last_name", "phone_number"]


@admin.register(models.Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ["id", "first_name", "last_name", "phone_number"]
    list_display_links = ["id", "first_name", "last_name", "phone_number"]


@admin.register(models.StudentSponsor)
class StudentSponsorAdmin(admin.ModelAdmin):
    list_display = ["id", "student", "allocated_amount"]
    list_display_links = ["id", "student", "allocated_amount"]

from django.core.validators import MinValueValidator
from rest_framework.exceptions import ValidationError
from django.db import models
from django.db.models import Sum, F
from rest_framework import serializers


class University(models.Model):
    university = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.university}"


class Sponsor(models.Model):
    LEGAL = "legal"
    PHYSICAL = "physical"
    SPONSOR_TYPE = (
        (LEGAL, "legal"),
        (PHYSICAL, "physical")
    )
    NEW = "new"
    ON_PROCESS = "on_process"
    APPROVED = "approved"
    CANCELLED = "cancelled"
    SPONSORSHIP_STATUS = (
        (NEW, "new"),
        (ON_PROCESS, "on_process"),
        (APPROVED, "approved"),
        (CANCELLED, "cancelled")
    )
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    middle_name = models.CharField(max_length=50, blank=True, null=True)
    phone_number = models.CharField(max_length=15, unique=True)
    organization = models.CharField(max_length=100, blank=True, null=True)
    allocated_amount = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0.0)])
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    type = models.CharField(max_length=20, choices=SPONSOR_TYPE)
    status = models.CharField(max_length=20, choices=SPONSORSHIP_STATUS)
    created_at = models.DateTimeField()

    class Meta:
        ordering = ['created_at']

    def get_full_name(self):
        return f'{self.first_name, self.last_name, self.middle_name}'


class Student(models.Model):
    BACHELOR = "bachelor"
    MASTER = "master"
    UNIV_DEGREE = (
        (BACHELOR, "bachelor"),
        (MASTER, "master")
    )
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    middle_name = models.CharField(max_length=50, blank=True, null=True)
    phone_number = models.CharField(max_length=15, unique=True)
    univ_degree = models.CharField(max_length=20, choices=UNIV_DEGREE)
    university = models.ForeignKey(to=University, on_delete=models.CASCADE, related_name="student_university")
    contract_amount = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0.0)])
    balance = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    created_at = models.DateTimeField()


class StudentSponsor(models.Model):
    student = models.ForeignKey(to=Student, on_delete=models.CASCADE, related_name="student")
    sponsor = models.ForeignKey(to=Sponsor, on_delete=models.CASCADE, related_name="sponsor")
    allocated_amount = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0.0)])

    def save(self, *args, **kwargs):
        if StudentSponsor.objects.filter(sponsor_id=self.sponsor.id, student_id=self.student.id):
            raise ValidationError(f'Sorry, this sponsor already sponsored this student.')
        if self.sponsor.balance > 0 and self.sponsor.balance >= self.allocated_amount:
            if (self.student.balance + self.allocated_amount) <= self.student.contract_amount:
                self.student.balance += self.allocated_amount
                self.student.save()
                self.sponsor.balance -= self.allocated_amount
                self.sponsor.save()
            elif (self.student.contract_amount - self.student.balance) > 0:
                left = self.student.contract_amount - self.student.balance
                raise ValidationError(f'Sorry, only {left} is enough.')
            else:
                raise ValidationError('Sorry, your balance is enough.')
        else:
            raise ValidationError('Sorry, your balance is not enough.')
        super(StudentSponsor, self).save(*args, **kwargs)
